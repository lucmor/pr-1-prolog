:- op(300,fx,~). % negaci´on, prefija, no asociativa.
:- op(400,yfx,(/\)). % conjunci´on, infija, asociativa a izquierda.
:- op(500,yfx,(\/)). % disyunci´on, infija, asociativa a izquierda.
:- op(600,xfx,=>). % implicaci´on, infija, no asociativa.
:- op(650,xfx,<=>). % equivalencia, infija, no asociativa.

/***************
 * Inicio FNCR *
 ***************/

/*
 * Convierte una FBF a FNCR
 *     1 - Cambiar los caracteres invalidos como => y <=>
 *     2 - Cambiar los ~ para que queden aplicados solamentes a literales
 *     3 - Cambiar los \/ para que queden solamente diyunciones y no mezclados con conjunciones
 *     4 - Cambiar los /\ para que queden conjunciones de diyunciones
 *     5 - Elimina las redundancias
 */
fncr(FBF, FNCR) :-
    fnc(FBF, FNC),
    fncAFncr(FNC, FNCR).

/**
 * Aplica las primeras 4 reglas 
 **/
fnc(FBF, FNC) :-
    transformarCaracteresInvalidos(FBF, FBFSinInvalidos),

    procesoCaracterNegativo(FBFSinInvalidos, FBFNegativosProcesados),

    procesoCaracterOr(FBFNegativosProcesados, FBFOrProcesado),

    procesoCaracterAnd(FBFOrProcesado, FBFAndProcesado),

    FNC = FBFAndProcesado.

/**
 * Elimina redundancias
 **/
fncAFncr(FNC, FNCR) :- 
    writeln(""), writeln(""), writeln(""),
    writeln("********** ELIMINO REDUNDANCIA **********"),
    write("con -> "), writeln(FNC),
    writeln(""), writeln(""), writeln(""),

    eliminarRedundancia(FNC, FNCR).

/**
 * Refuta una fnc
 *     1 - Pasa la fnc a una lista de listas
 *     2 - Genera resolventes
 *     3 - Chequea si existe bottom
 **/
refutable(Fnc) :-
    refutableAuxiliar(Fnc, Salida), 
    chequeoDeBottomRefutable(Salida).

refutableAuxiliar(Fnc, Salida) :- 
    crearConjunto(Fnc, Lista),
    generarResolventes(Lista, Lista, Salida).

chequeoDeBottomRefutable(Lista) :-
    member(bottom, Lista), 
    writeln("ES REFUTABLE").

chequeoDeBottomRefutable(_) :- 
    writeln("NO ES REFUTABLE").


/*
Teorema 
    · Recibir FBF
    · Obtener FNCR a partir de ~(FBF)
    teorema -> refutable(~FNCR).
*/
teorema(FBF) :-
    fncr(~FBF, FNCRNegada),
    refutableAuxiliar(FNCRNegada, Salida),
    write(Salida),
    chequeoDeBottomTeorema(Salida).

chequeoDeBottomTeorema(Lista) :-
    member(bottom, Lista), 
    writeln(""), writeln(""), 
    writeln("ES UN TEOREMA").

chequeoDeBottomTeorema(_) :- 
    writeln(""), writeln(""), 
    writeln("NO ES UN TEOREMA").

/*******************************************
 * Inicio de cambio de caracteres => y <=> *
 *******************************************/

% Cambia la aparicion de A <=> B por ((~A \/ B) /\ (A \/ ~B))
transformarCaracteresInvalidos(ParteIzquierda <=> ParteDerecha, FNC) :-
    write("Procesa: "), write(ParteIzquierda), write(" <=> "), write(ParteDerecha), writeln(""), 

    fnc((~ParteIzquierda \/ ParteDerecha) /\ (ParteIzquierda \/ ~ParteDerecha), FNCProcesado),

    write("Resultado:  "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Cambia la aparicion de A => B por ~A \/ B
transformarCaracteresInvalidos(ParteIzquierda => ParteDerecha, FNC) :-
    write("Procesa: "), write(ParteIzquierda), write(" => "), write(ParteDerecha), writeln(""),

    fnc(~ParteIzquierda \/ ParteDerecha, FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

transformarCaracteresInvalidos(FBF, FBF).

/****************************************
 * Fin de cambio de caracteres => y <=> *
 ****************************************/

/*********************************
 * Inicio de reglas del negativo *
 *********************************/

% Simplifica si ~~A por A
procesoCaracterNegativo(~(~ParteDerecha), FNC) :-
    write("Procesa: "), write("~(~"), write(ParteDerecha), writeln(")"), writeln(""),

    fnc(ParteDerecha, FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Aplica DeMorgan ~(A \/ B) por ~A /\ ~B
procesoCaracterNegativo(~(ParteIzquierda \/ ParteDerecha), FNC) :-
    write("Procesa: "), write("~("), write(ParteIzquierda), write(" \\/ "), write(ParteDerecha), writeln(")"), writeln(""),

    fnc(~ParteIzquierda /\ ~ParteDerecha, FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Aplica DeMorgan ~(A /\ B) por ~A \/ ~B
procesoCaracterNegativo(~(ParteIzquierda /\ ParteDerecha), FNC) :-
    write("Procesa: "), write("~("), write(ParteIzquierda), write(" /\\ "), write(ParteDerecha), writeln(")"), writeln(""),

    fnc(~ParteIzquierda \/ ~ParteDerecha, FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Cambia ~bottom por top
procesoCaracterNegativo(~bottom, FNC) :-
    write("Procesa: "), write("~bottom "), writeln(""),

    writeln("Resultado: top"), writeln(""),

    FNC = top.

% Cambia ~top por bottom
procesoCaracterNegativo(~top, FNC) :-
    write("Procesa: "), writeln("~top"), writeln(""),

    writeln("Resultado: bottom"), writeln(""),

    FNC = bottom.

% Caso especial si recibe cambia ~(A => B) por ~(~A \/ B)
procesoCaracterNegativo(~(ParteIzquierda => ParteDerecha), FNC) :-
    write("Procesa: "), write("~("), write(ParteIzquierda), write(" => "), write(ParteDerecha), writeln(")"), writeln(""),

    transformarCaracteresInvalidos(ParteIzquierda => ParteDerecha, ParteValido),
    
    fnc(~(ParteValido), FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Caso especial si recibe cambia ~(A <=> B) por ~((~A \/ B) /\ (A \/ ~B))
procesoCaracterNegativo(~(ParteIzquierda <=> ParteDerecha), FNC) :-
    write("Procesa: "), write("~("), write(ParteIzquierda), write(" <=> "), write(ParteDerecha), writeln(")"), writeln(""),

    transformarCaracteresInvalidos(ParteIzquierda <=> ParteDerecha, ParteValido),
    
    fnc(~(ParteValido), FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

procesoCaracterNegativo(FBF, FBF).

/******************************
 * Fin de reglas del negativo *
 ******************************/

/***************************
 * Inicio de reglas del or *
 ***************************/

% Si la precendencia es a derecha cambio a izquierda A \/ (B \/ C) por (A \/ B) \/ C
procesoCaracterOr(ParteIzquierda \/ (ParteDerechaIzquierda \/ ParteDerechaDerecha), FNC) :-
    fnc((ParteIzquierda \/ ParteDerechaIzquierda) \/ ParteDerechaDerecha, FNC).

% En caso de tener mal la precedencia al tener A /\ B \/ C \/ D le agrega parentesis para A /\ (B \/ C \/ D)
procesoCaracterOr((ParteIzquierdaIzquierda /\ ParteIzquierdaDerecha1 \/ ParteIzquierdaDerecha2) \/ ParteDerecha, FNC) :-     
    fnc(ParteIzquierdaIzquierda /\ (ParteIzquierdaDerecha1 \/ ParteIzquierdaDerecha2 \/ ParteDerecha), FNC).

% En caso de tener mal la precedencia al tener A \/ B /\ C /\ D le agrega parentesis para (A \/ B) /\ C /\ D)
procesoCaracterOr(ParteIzquierda \/ (ParteDerechaIzquierda /\ ParteDerechaCentro /\ ParteDerechaDerecha), FNC) :-     
    fnc((ParteIzquierda \/ ParteDerechaIzquierda) /\ ParteDerechaCentro /\ ParteDerechaDerecha, FNC).

% Distributiva A \/ (B /\ C) a (A \/ B) /\ (A \/ B)
procesoCaracterOr(ParteIzquierda \/ (ParteDerechaIzquierda /\ ParteDerechaDerecha), FNC) :-
    write("Procesa: "),
    write(ParteIzquierda), write(" \\/ ("), 
    write(ParteDerechaIzquierda), write(" /\\ "), write(ParteDerechaDerecha), writeln(") --> "), writeln(""),

    fnc((ParteIzquierda \/ ParteDerechaIzquierda) /\ (ParteIzquierda \/ ParteDerechaDerecha), FNCProcesado),

    write("Resultado: "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Distributiva (A /\ B) \/ C a (A \/ C) /\ (B \/ C)
procesoCaracterOr((ParteIzquierdaIzquierda /\ ParteIzquierdaDerecha) \/ ParteDerecha, FNC) :- 
    write("Procesa: "),
    write("("), write(ParteIzquierdaIzquierda), write(" /\\ "), write(ParteIzquierdaDerecha), 
    write(") \\/ "), write(ParteDerecha), writeln(" --> "), writeln(""),

    fnc((ParteIzquierdaIzquierda \/ ParteDerecha) /\ (ParteIzquierdaDerecha \/ ParteDerecha), FNCProcesado),

    write("Resultado:  "), writeln(FNCProcesado), writeln(""),

    FNC = FNCProcesado.

% Aplica fnc A los 2 lados de A \/ B
procesoCaracterOr(ParteIzquierda \/ ParteDerecha, FNC) :- 
    write("Procesa: "),
    write(ParteIzquierda), write(" \\/ "), write(ParteDerecha), writeln(" --> "), writeln(""),
    
    fnc(ParteIzquierda, FNCIzquierdaProcesado),
    fnc(ParteDerecha, FNCDerechaProcesado),
    
    write("Resultado: "), write(FNCIzquierdaProcesado), write(" \\/ "), writeln(FNCDerechaProcesado), writeln(""),

    FNC = FNCIzquierdaProcesado \/ FNCDerechaProcesado.

procesoCaracterOr(FBF, FBF).

/******************************
 *    Fin de reglas del or   *
 ******************************/

/****************************
 * Inicio de reglas del and *
 ****************************/

% Si la precendencia es a derecha cambio a izquierda A \/ (B \/ C) por (A \/ B) \/ C
procesoCaracterAnd(ParteIzquierda /\ (ParteDerechaIzquierda /\ ParteDerechaDerecha), FNC) :-
    fnc((ParteIzquierda /\ ParteDerechaIzquierda) /\ ParteDerechaDerecha, FNC).

% Aplica fnc a los 2 lados A /\ B
procesoCaracterAnd(ParteIzquierda /\ ParteDerecha, FNC) :- 
    write("Procesa: "),
    write(ParteIzquierda), write(" /\\ "), write(ParteDerecha), writeln(" --> "), writeln(""),
    
    fnc(ParteIzquierda, FNCIzquierdaProcesado),
    fnc(ParteDerecha, FNCDerechaProcesado),

    write("Resultado: "), write(FNCIzquierdaProcesado), write(" /\\ "), writeln(FNCDerechaProcesado), writeln(""),

    FNC = FNCIzquierdaProcesado /\ FNCDerechaProcesado.

procesoCaracterAnd(FBF, FBF).

/****************************
 *  Fin de reglas del and   *
 ****************************/

% Crea una lista basado en los \/
crearConjuntoOr(ParteIzquierda\/ParteDerecha, [ParteDerecha | X]) :- crearConjuntoOr(ParteIzquierda, X).
crearConjuntoOr(Parte, [Parte]).

% Crea lista de lista de \/
crearListasDeOrDesdeAnd([X], [Lista]) :-
    crearConjuntoOr(X, Lista).

crearListasDeOrDesdeAnd([X | S], ListaAndConListaOr) :-
    crearListasDeOrDesdeAnd(S, ListaAndConListaOrS),
    crearConjuntoOr(X, ListaOr),
    ListaAndConListaOr = [ListaOr | ListaAndConListaOrS].

/**
 * Elimina la redundancia de una fnc y devuelve una fncr
 *     1 - Crea lista de /\. ej: [a\/b, c\/d]
 *     2 - Crea lista de \/. ej: [[a, b], [c, d]]
 *     3 - Elimina redundancias en las listas de mas adentro. ej: [[a, b, a], [c,d]] por [[a,b], [c,d]]
 *     4 - Elimina redundancias en las listas de mas afuera. ej: [[a], [~a], [b,c]] por [[b, c]]
 *     5 - Pasa de la lista a una fncr
 **/
eliminarRedundancia(FNC, FNCR) :-
    crearConjunto(FNC, ListaAnd),

    crearListasDeOrDesdeAnd(ListaAnd, ListaAndConListaOr),

    eliminarRedundanciaAnd(ListaAndConListaOr, ListaSinRedundancias),
    
    eliminarRedundanciaEntreAnd(ListaSinRedundancias, ListaPreProcesada),

    procesarListaFNCSinRedundancia(ListaPreProcesada, FNCR),
    
    writeln(""),writeln(""),writeln("*******FNCR*******"),writeln(FNCR),writeln(""),writeln("").

% Elimina redundancias en X que es una lista de \/
eliminarRedundanciaAnd([X], [ListaAndSinRedundancia]) :-
    eliminarRedundanciaOr(X, ListaOrSinRedundancia),
    
    ListaAndSinRedundancia = ListaOrSinRedundancia.

% Elimina redundancias en X que es una lista de \/ y vuelve a procesar en caso de que quede otras listas
eliminarRedundanciaAnd([X | S], Salida) :- 
    eliminarRedundanciaAnd(S, ListaSOrSinRedundancia),
    eliminarRedundanciaOr(X, ListaOrSinRedundancia),
    sacarRedundanciasListaAnd(ListaOrSinRedundancia, ListaSOrSinRedundancia, ListaAndSinRedundancia),

    procesarSalidaRedundanciaAnd(ListaAndSinRedundancia, ListaAndAuxiliar),
    Salida = ListaAndAuxiliar.

eliminarRedundanciaEntreAnd([X], [X]).

% Buscar si existe un complementarios en las listas de \/ si X es un literal
eliminarRedundanciaEntreAnd([X | S], ListaDeAnds) :-
    eliminarRedundanciaEntreAnd(S, ListaDeAndsRecursiva),
    buscarComplementarioListaAnds(X, ListaDeAndsRecursiva, SalidaProcesar),
    ListaDeAnds = SalidaProcesar.

eliminarRedundanciaOr([X], [X]).

% Si existe el mismo literal devuelve lo que queda de la lista
eliminarRedundanciaOr([X | S], F) :-
    member(X, S),
    eliminarRedundanciaOr(S, Salida),
    F = Salida.

% Si existe el literal complementario devuelve una lista con un top
eliminarRedundanciaOr([~X | S], F) :-
    member(X, S),
    F = [top].

% Si existe el literal complementario devuelve una lista con un top
eliminarRedundanciaOr([X | S], F) :-
    member(~X, S),
    F = [top].

% Si no encuentra nada sigue con el resto de la lista
eliminarRedundanciaOr([X | S], F) :-
    eliminarRedundanciaOr(S, Salida),
    F = [X | Salida].

% Si encuentra un top en la lista lo ignora
procesarSalidaRedundanciaAnd([A], []) :-
    member(top, A).

% Si no encuentra un top en la lista devuelve el mismo 
procesarSalidaRedundanciaAnd([A], [A]).

% Si encuentra un top en la lista lo ignora de la lista
procesarSalidaRedundanciaAnd([X | S], S) :-
    member(top, X).

% Si no encuentra un top en la lista devuelve la misma lista
procesarSalidaRedundanciaAnd([X | S], Salida) :-
    procesarSalidaRedundanciaAnd(S, SalidaS),
    Salida = [X | SalidaS].

% procesamos la lista de listas
procesarListaFNCSinRedundancia([X], Salida) :-
    procesarListaFNCSinRedundanciaOr(X, Salida).

% Uno la lista de /\
procesarListaFNCSinRedundancia([X | S], FNCR) :- 
    procesarListaFNCSinRedundancia(S, SProcesada),
    procesarListaFNCSinRedundanciaOr(X, XProcesada),
    FNCR = SProcesada /\ XProcesada.

procesarListaFNCSinRedundanciaOr([X], X).

% Uno la lista de \/
procesarListaFNCSinRedundanciaOr([X | S], FNCR) :-
    procesarListaFNCSinRedundanciaOr(S, SProcesada),
    FNCR = SProcesada \/ X.

% Busca si existe el complementario del literal en la lista de ands y devuelve un bottom
buscarComplementarioListaAnds(Literal, ListaDeAnds, Salida) :-
    buscarComplementarioEnListaAnd(Literal, ListaDeAnds),
    Salida = [[bottom]].

% Busca si existe el complementario del literal en la lista de ands y devuelve un bottom
buscarComplementarioListaAnds(_, ListaDeAnds, Salida) :-
    member([bottom], ListaDeAnds),
    Salida = [[bottom]].

buscarComplementarioListaAnds(Literal, ListaDeAnds, Salida) :-
    Salida = [Literal | ListaDeAnds].

% Revisa si la lista con el literal complementario pertenece a la lista de ands
buscarComplementarioEnListaAnd([~Literal], ListaDeAnds) :-
    member([Literal], ListaDeAnds).

% Revisa si la lista con el literal complementario pertenece a la lista de ands
buscarComplementarioEnListaAnd([Literal], ListaDeAnds) :-
    member([~Literal], ListaDeAnds).

% Si encuentra la misma lista de literales la ignora
sacarRedundanciasListaAnd(Clausula, ListaAnds, Salida) :-
    member(Clausula, ListaAnds),
    Salida = ListaAnds.

sacarRedundanciasListaAnd(Clausula, ListaAnds, Salida) :- 
    Salida = [Clausula | ListaAnds].

/*************************************
 * Fin de eliminacion de redundancia *
 *************************************/

/***
 * Refutabilidad
 **/

% Crea un conjunto de /\
crearConjunto(ParteIzquierda /\ ParteDerecha, [ParteDerecha | X]) :- crearConjunto(ParteIzquierda, X).
crearConjunto(Parte, [Parte]).

% Pertenece a una lista
member(X, [X | _]).
member(X, [_ | T]) :- member(X, T).

% Concatena 2 listas
concat([], Xs, Xs).
concat([X | Xs], Ys, [X | Zs]) :- concat(Xs, Ys, Zs).

% Si la resolvente encontrada no pertenece al set los concatena
encontroResolvente(Resolvente, Set, Salida) :-
    \+member(Resolvente, Set),
    concat([Resolvente], Set, Salida).

encontroResolvente(_, Set, Set). 

% Genera resolventes y verifica si hay nuevas vuelve a generarlas
generarResolventes(Set, Set, Salida) :- 
    ciclarClausulas(Set, Set, SalidaClausulas),
    controlCicloDeClausulas(Set, SalidaClausulas, Salida).
    
generarResolventes([], Set, Set).
generarResolventes(_, Set, Set).

controlCicloDeClausulas(Set, SalidaClausulas, Salida) :-
    Set \= SalidaClausulas, 
    generarResolventes(SalidaClausulas, SalidaClausulas, Salida).

controlCicloDeClausulas(_,SalidaClausulas,SalidaClausulas).

% Por cada clausula busca resolventes y en el caso de encontrar un bottom para
ciclarClausulas(Lista, Set, Set) :-
    member(bottom, Lista).

ciclarClausulas([Clausula | S], Set, Salida) :-
    ciclarClausulas(S, Set, SalidaClausulas),
    buscarComplementario(Clausula, Set, Resolvente),
    encontroResolvente(Resolvente, SalidaClausulas, Salida).

ciclarClausulas([_], Set, Set).

% Si la clausula no es la cabeza de la lista busca complementario
buscarComplementario(Clausula, [X | _], SalidaX) :-
    Clausula \= X,
    procesarComplementario(Clausula, X, SalidaX).

buscarComplementario(Clausula, [_ | S], Salida) :-
    buscarComplementario(Clausula, S, Salida).

% Si encuentra un complementario, saca los literales y devuelve la resolvente
procesarComplementario(ClausulaIzquierda \/ ClausulaDerecha, X, Salida) :-
    existeComplementario(ClausulaDerecha, X),
    clausulaSinLiteral(ClausulaDerecha, X, ClausulaSinLiteral),
    juntarClausulas(ClausulaIzquierda, ClausulaSinLiteral, ClausulasUnidas),
    arreglarPrecedencia(ClausulasUnidas, ClausulaBienPrecedencia),
    arreglarRedundancia(ClausulaBienPrecedencia, ClausulaSinRedundancia),
    Salida = ClausulaSinRedundancia.

% Si encuentra un complementario, saca los literales y devuelve la resolvente
procesarComplementario(ClausulaIzquierda \/ ClausulaDerecha, X, Salida) :-
    existeComplementario(ClausulaIzquierda, X),
    clausulaSinLiteral(ClausulaIzquierda, X, ClausulaSinLiteral),
    juntarClausulas(ClausulaSinLiteral, ClausulaDerecha, ClausulasUnidas),
    arreglarPrecedencia(ClausulasUnidas, ClausulaBienPrecedencia),
    arreglarRedundancia(ClausulaBienPrecedencia, ClausulaSinRedundancia),
    Salida = ClausulaSinRedundancia.

procesarComplementario(ClausulaIzquierda \/ ClausulaDerecha, X, Salida) :-
    procesarComplementario(ClausulaIzquierda, X, SalidaAuxiliar),
    juntarClausulas(SalidaAuxiliar, ClausulaDerecha, Salida).

procesarComplementario(ClausulaIzquierda \/ ClausulaDerecha, X, Salida) :-
    procesarComplementario(ClausulaDerecha, X, SalidaAuxiliar),
    juntarClausulas(ClausulaIzquierda, SalidaAuxiliar, Salida).

procesarComplementario(Literal, XIzquierda \/ XDerecha, Salida) :-
    existeComplementario(Literal, XIzquierda \/ XDerecha),
    clausulaSinLiteral(Literal, XIzquierda \/ XDerecha, ClausulaSinLiteral),
    arreglarPrecedencia(ClausulaSinLiteral, ClausulaBienPrecedencia),
    arreglarRedundancia(ClausulaBienPrecedencia, ClausulaSinRedundancia),
    Salida = ClausulaSinRedundancia.

procesarComplementario(~Literal, Clausula, bottom) :-
    Literal == Clausula.

procesarComplementario(Literal, ~Clausula, bottom) :-
    Literal == Clausula.

% Devuelve la clausula sin el literal complementario
clausulaSinLiteral(~Literal, ClausulaIzquierda \/ ClausulaDerecha, ClausulaIzquierda) :-
    Literal == ClausulaDerecha.

clausulaSinLiteral(Literal, ClausulaIzquierda \/ ~ClausulaDerecha, ClausulaIzquierda) :-
    Literal == ClausulaDerecha.

clausulaSinLiteral(~Literal, ClausulaIzquierda \/ ClausulaDerecha, ClausulaDerecha) :-
    Literal == ClausulaIzquierda.

clausulaSinLiteral(Literal, ~ClausulaIzquierda \/ ClausulaDerecha, ClausulaDerecha) :-
    Literal == ClausulaIzquierda.

clausulaSinLiteral(Literal, ClausulaIzquierda \/ ClausulaDerecha, Salida) :-
    clausulaSinLiteral(Literal, ClausulaIzquierda, SalidaAuxiliar),
    Salida = SalidaAuxiliar \/ ClausulaDerecha.

% Si hay un complementario den la clausula
existeComplementario(Literal, _ \/ ClausulaDerecha) :-
    esComplementario(Literal, ClausulaDerecha).

existeComplementario(Literal, ClausulaIzquierda \/ _) :-
    esComplementario(Literal, ClausulaIzquierda).

existeComplementario(Literal, ClausulaIzquierda \/ _) :-
    existeComplementario(Literal, ClausulaIzquierda).
                         
% Si el literal es el complementario
esComplementario(~Literal, LiteralClausula) :-
    Literal == LiteralClausula.

esComplementario(Literal, ~LiteralClausula) :-
    Literal == LiteralClausula.

% Junta las clausulas ignorandos los bottoms
juntarClausulas(bottom, Derecha, Derecha).
juntarClausulas(Izquierda, bottom, Izquierda).
juntarClausulas(A\/B, bottom, Salida) :- juntarClausulas(A, B, Salida).
juntarClausulas(A\/B, C, Salida) :- juntarClausulas(A, B, SalidaAuxiliar), Salida = SalidaAuxiliar \/ C.
juntarClausulas(bottom, B \/ C, Salida) :- juntarClausulas(B, C, Salida).
juntarClausulas(A, B \/ C, Salida) :- juntarClausulas(B, C, SalidaAuxiliar), Salida = A \/ SalidaAuxiliar.
juntarClausulas(A, A, A).
juntarClausulas(Izquierda, Derecha, Izquierda \/ Derecha).

% En caso de tener mal la precedencia la acomoda
arreglarPrecedencia(A \/ (B \/ C), (A \/ B) \/ C).
arreglarPrecedencia(FBF, FBF).

% En caso de tener literales redundantes iguales los simplifica
arreglarRedundancia(ParteIzquierda \/ ParteDerecha, ParteDerecha) :-
    ParteIzquierda == ParteDerecha.
arreglarRedundancia(ParteIzquierda \/ _, Salida) :-
    arreglarRedundancia(ParteIzquierda, Salida).
arreglarRedundancia(FBF, FBF).
