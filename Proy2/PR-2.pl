% :- use_rendering(table).
% esto aca no funciona pq es porqueria de SWISH
% ajo y agua

% desplazar(+Dir, +Num, +Cant, +Tablero, -EvolTablero).
% Desplaza la fila o columna Num de Tablero, Cant veces en direccion Dir,
%   luego ejecuta las normas del juego y devuelve en EvolTablero una evolución del juego.
desplazar(Dir, Num, Cant, Tablero, EvolTablero) :-
    writeln("\t\t Tablero inicial: "), imprimirTablero(Tablero), writeln(""),

    desplazamiento(Dir, Num, Cant, Tablero, SalidaDesplazamiento),

    writeln("\t\t Tablero desplazado: "), imprimirTablero(SalidaDesplazamiento), writeln(""),

    colapsar(SalidaDesplazamiento, Dir, Num, SalidaColapso),

    huboDesplazo(SalidaDesplazamiento, SalidaColapso, SalidaHuboDesplazo),

    concat([SalidaDesplazamiento], SalidaHuboDesplazo, EvolTablero).

huboDesplazo(TableroDesplazado, TableroColapsado, Salida) :-

    TableroDesplazado \= TableroColapsado,

    writeln("\t\t Tablero colapsado: "), imprimirTablero(TableroColapsado), writeln(""),

    gravedad(TableroColapsado, SalidaGravedad),

    writeln("\t\t Tablero gravitacionado: "), imprimirTablero(SalidaGravedad), writeln(""),

    rellenar(SalidaGravedad, SalidaRelleno),

    writeln("\t\t Tablero rellenado: "), imprimirTablero(SalidaRelleno), writeln(""),

    Salida = [TableroColapsado, SalidaGravedad, SalidaRelleno].

huboDesplazo(TableroDesplazado, _, TableroDesplazado).

% desplazamiento(+Dir, +Num, +Cant, +Tablero, -TableroDesplazado)
% desplaza la fila o columna Num de Tablero, Cant veces en direccion Dir,
%   y devuelve el tablero resultante en TableroDesplazado.
desplazamiento(Dir, Num, Cant, Tablero, TableroDesplazado) :-
    Dir \= abajo, Dir \= arriba,
    desplazamientoHorizontal(Dir, Num, Cant, Tablero, TableroDesplazado).

desplazamiento(Dir, Num, Cant, Tablero, TableroDesplazado) :-
    desplazamientoVertical(Tablero, Dir, Cant, Num, TableroDesplazado).

% colapsar(+TableroDesplazado, +Dir, +Num, -TableroColapsado).
% Colapsa grupos de a tres elementos iguales dentro del tablero,
%   y devuelve el resultado en TableroColapsado.
colapsar(TableroDesplazado, Dir, Num, TableroColapsado) :-
    Dir \= abajo, Dir \= arriba,
    colapsarMovimientoFila(TableroDesplazado, Num, TableroColapsado).

colapsar(TableroDesplazado, _, Num, TableroColapsado) :-
    colapsarMovimientoColumna(TableroDesplazado, Num, TableroColapsado).

% gravedad(+TableroColapsado, -TableroCaido).
% Produce la caida de elementos de TableroColapsado, y devuelve el resultado en TableroCaido.
gravedad(TableroColapsado, TableroCaido) :-
    length(TableroColapsado, LongitudTablero),
    
    trasponer(TableroColapsado, LongitudTablero, TableroTraspuesto), 
    gravedadEnTablero(TableroTraspuesto, SalidaGravedad),
    trasponer(SalidaGravedad, LongitudTablero, SalidaGravedadTraspuesta),

    TableroCaido = SalidaGravedadTraspuesta.

% rellenar(+TableroCaido, -TableroRelleno).
% Rellena TableroCaido con elementos aleatorios, y devuelve el resultado en TableroRelleno.
rellenar(TableroCaido, TableroRelleno) :-
    length(TableroCaido, L),
    rellenarCascara(TableroCaido, L, TableroRelleno).

/**
 * Predicados auxiliares para operar con los elementos del tablero
 **/

% evolucion(+X, -Y).
% Obtiene el elemento evolucionado de X y lo devuelve en Y.
evolucion(X, Y) :-
    L = [a1, a2, a3, v1, v2, v3, r1, r2, r3],
    encontrarPrimerAparicion(X, L, I),
    I mod 3 \= 0, 
    memberIn(I+1, L, Y).

evolucion(X, X).

% obtenerElementoAleatorio(-Salida).
% Obtiene un elemento aleatorio para agregar al tablero al momento de rellenar.
obtenerElementoAleatorio(Salida) :-
    L = [a1, v1, r1],
    length(L, Long),
    random_between(1, Long, Rand),
    memberIn(Rand, L, Salida).


/**
 * Predicados auxiliares para operar con listas
 **/

% concat(+Lista1, +Lista2, -Lista3).
% Concatena Lista1 y Lista 2, y devuelve el resultado en Lista3.
concat([], Xs, Xs).
concat([X | Xs], Ys, [X | Zs]) :- concat(Xs, Ys, Zs).

% member(+Elemento, +Lista).
% Devuelve verdadero si Elemento pertenece a Lista, falso en caso contrario
member(X, [X | _]).
member(X, [_ | T]) :- member(X, T).

% reversaLista(+Lista, -Salida).
% Invierte la Lista y devuelve el resultado en Salida.
reversaLista(Lista, Salida) :- auxReversa(Lista, [], Salida).

% auxReversa(+Lista, +Acumulador, -Salida).
% Metodo auxiliar recursivo para efectuar la inversion de Lista.
auxReversa([H | T], A, R) :- auxReversa(T, [H | A], R).
auxReversa([], A, A). 

% copiarNElementos(+Fin, +Lista, -SalidaAntes, -SalidaDespues).
% Copia los elementos de Lista, desde el inicio hasta el indice Fin, y los devuelve
%   en una nueva lista SalidaAntes y devuelve lo que queda en SalidaDespues.
copiarNElementos(0, List, [], List).
copiarNElementos(1, [X | Xs], [X], Xs).
copiarNElementos(N, [X | Xs], SalidaAntesX, SalidaDespuesX) :-
    NNuevo is N - 1,
    copiarNElementos(NNuevo, Xs, SalidaAuxAntes, SalidaDespuesX),
    concat([X], SalidaAuxAntes, SalidaAntesX).

% memberIn(+Index, +List, -Value).
% Busca el miembro numero Index en Lista y lo devuelve en Value.
memberIn(Index, [X | _], X) :- Index == 1.
memberIn(Index, [_ | S], Salida) :- 
    Index \= 1,
    IndexNuevo is Index - 1,
    memberIn(IndexNuevo, S, Salida).

% memberSeguido(+Lista, +Elemento, -Cantidad).
% Recorre Lista, busca apariciones continuas de Elemento 
%   y devuelve cuantas hay en Cantidad.
memberSeguido([X], Elemento, 1) :- Elemento == X.

memberSeguido([X], Elemento, 0) :- Elemento \= X.

memberSeguido([X | T], Elemento, Cantidad) :-
    Elemento == X,
    memberSeguido(T, Elemento, CantidadAux), 
    Cantidad is CantidadAux + 1.

memberSeguido([X | _], Elemento, 0) :-
    Elemento \= X.

% encontrarUltimaAparicion(+Value, +List, -Index).
% Encuentra la ultima aparicion de Value en List y devuelve el resultado en Index.
encontrarUltimaAparicion(Value, List, Index) :- 
    reversaLista(List, ReverseList),
    encontrarPrimerAparicion(Value, ReverseList, IndexAux),
    length(List, Length),
    Index is Length - IndexAux + 1. 

% encontrarPrimerAparicion(+Value, +List, -Index).
% Encuentra la primer aparicion de Value en List y y devuelve el resultado en Index.
encontrarPrimerAparicion(Value, [Value | _], Index) :- Index is 1.
encontrarPrimerAparicion(Value, [_ | Xs], Index) :- 
    encontrarPrimerAparicion(Value, Xs, IndexRecursivo),
    Index is IndexRecursivo + 1.

% replace(+Index, +List, +Value, -NewList).
% Reemplaza en List al valor ubicado en Index por Value y devuelve el resultado en NewList.
replace(Index, [_ | S], Value, [Value | S]) :- Index == 1.
replace(Index, [X | S], Value, Salida) :- 
    Index \= 1,
    IndexNuevo is Index - 1,
    replace(IndexNuevo, S, Value, SalidaRecursiva),
    concat([X], SalidaRecursiva, Salida).
    
% clon(+List, -Salida).
% clona la lista List y la devuelve en Salida.
clon(List, Salida) :- concat([], List, Salida).

/**
 * Predicados auxiliares para operar con el tablero
 **/

% trasponer(+Tablero, +Num, -Salida).
% Traspone Tablero de manera recursiva y devuelve el resultado en Salida.
trasponer(Tablero, 1, Salida) :-
    armarListaDeColumna(Tablero, 1, ListaColumna),
    Salida = [ListaColumna].

trasponer(Tablero, Num, Salida) :- 
    armarListaDeColumna(Tablero, Num, ListaColumna),
    NumSiguiente is Num - 1,
    trasponer(Tablero, NumSiguiente, SalidaRecursiva),
    concat(SalidaRecursiva, [ListaColumna], Salida).

% imprimirTablero(+Tablero).
% Imprime cada componente de tablero bien.
imprimirTablero(Tablero) :- 
    forall(member(M, Tablero), writeln(M)).

/**
 * Predicados auxiliares para realizar el desplazamiento
 **/

% armarListaDeColumna(+Tablero, +Num, -Salida).
% Arma una lista con los elementos de la columna Num de Tablero y lo devuelve en Salida.
armarListaDeColumna([Fila], Num, Salida) :- 
    memberIn(Num, Fila, Value),
    Salida = [Value].

armarListaDeColumna([Fila | FilasRestantes], Num, Salida) :- 
    armarListaDeColumna(FilasRestantes, Num, SalidaRecursiva),
    memberIn(Num, Fila, Value),
    concat([Value], SalidaRecursiva, Salida).

% reemplazarListaDeColumna(+Tablero, +Valores, +Num, -Salida).
% Reemplaza los elementos de la columna Num de Tablero por los Valores correspondientes y lo devuelve en Salida.
reemplazarListaDeColumna([Fila | FilasRestantes], [ValorFila1 | ValoresRestantes], Num, Salida) :-
    replace(Num, Fila, ValorFila1, SalidaReemplazo),
    reemplazarListaDeColumna(FilasRestantes, ValoresRestantes, Num, SalidaRecursiva),
    concat([SalidaReemplazo], SalidaRecursiva, Salida).
    
reemplazarListaDeColumna([Fila], [ValorFila], Num, Salida) :-
    replace(Num, Fila, ValorFila, SalidaReemplazo),
    Salida = [SalidaReemplazo].

% cambiarDireccionVertical(+In, -Out).
% Intercambia las direcciones arriba o abajo por izq o der segun corresponda.
cambiarDireccionVertical(abajo,der).
cambiarDireccionVertical(arriba,izq).

/**
 * Desplazamiento
 **/

% desplazamientoHorizontal(+Dir, +Num, +Cant, +Tablero, -TableroDesplazado).
% Efectua el desplazamiento de una fila.
desplazamientoHorizontal(Dir, Num, Cant, Tablero, TableroDesplazado) :-
    clon(Tablero, TableroClonado),
    memberIn(Num, TableroClonado, Fila),
    desplazamientoDeLista(Dir, Cant, Fila, FilaDesplazada),
    replace(Num, TableroClonado, FilaDesplazada, SalidaReemplazo),

    TableroDesplazado = SalidaReemplazo.

% desplazamientoVertical(+Tablero, +Dir, +Cant, +Num, -Salida).
% Toma la columna numero Num de Tablero, arma una lista con los valores alli ubicados, 
%   luego desplaza y devuelve el tablero resultante en Salida.
desplazamientoVertical(Tablero, Dir, Cant, Num, Salida) :-
    armarListaDeColumna(Tablero, Num, ListaColumna),
    cambiarDireccionVertical(Dir, DirNueva),
    desplazamientoDeLista(DirNueva, Cant, ListaColumna, SalidaDesplazamiento),
    reemplazarListaDeColumna(Tablero, SalidaDesplazamiento, Num, TableroFinal),
    Salida = TableroFinal.

% desplazamientoDeLista(+Dir, +Cant, +Lista, -Salida).
% Desplaza Lista segun Dir, Cant veces y devuelve la lista resultante en Salida.
desplazamientoDeLista(Dir, Cant, Lista, Salida) :- 
    Cant == 1,
    controlDireccion(Dir, Lista, SalidaAux),
    Salida = SalidaAux.

desplazamientoDeLista(Dir, Cant, Lista, Salida) :-
    controlDireccion(Dir, Lista, SalidaAux),
    NuevaCant is Cant - 1,
    desplazamientoDeLista(Dir, NuevaCant, SalidaAux, Salida).

% controlDireccion(+Dir, +List, -Salida).
% Realiza una rotacion de List en el sentido indicado por Dir y lo devuelve en Salida.
controlDireccion(Dir, List, Salida) :-
    Dir == der,
    length(List, Length),
    Ultimo is Length-1,
    copiarNElementos(Ultimo, List, PrimerosN, UltimoElemento),
    concat(UltimoElemento, PrimerosN, Salida).

controlDireccion(_, List, Salida) :-
    % si Dir no es der entonces es izq
    copiarNElementos(1, List, PrimerN, ElementosRestantes),
    concat(ElementosRestantes, PrimerN, Salida).


/**
 * Colapso
 **/

% colapsarMovimientoFila(+Tablero, +Num, -Salida).
% Chequea la fila Num de Tablero, y efectua los colapsos de 
%   la fila y de todas las columnas.
colapsarMovimientoFila(Tablero, Num, Salida) :-
    chequeoFila(Tablero, Num, SalidaChequeoFila),    

    length(SalidaChequeoFila, L),
    trasponer(SalidaChequeoFila, L, TableroTraspuesto),
    chequeoColumnas(TableroTraspuesto, L, SalidaChequeoColumnas),
    trasponer(SalidaChequeoColumnas, L, SalidaReal),

    Salida = SalidaReal. 

colapsarMovimientoColumna(Tablero, Num, Salida) :-
    length(Tablero, Longitud),
    trasponer(Tablero, Longitud, TableroTraspuesto),

    colapsarMovimientoFila(TableroTraspuesto, Num, TableroTraspuestoSalida),

    trasponer(TableroTraspuestoSalida, Longitud, SalidaTablero),

    Salida = SalidaTablero.

% chequeoFila(+Tablero, +Num, -Salida).
% Controla la fila Num de Tablero, y si contiene un grupo de 3 o mas
%   efectua un colapso cruzado.
chequeoFila(Tablero, Num, Salida) :-
    memberIn(Num, Tablero, Fila),
    chequeoGrupo(Tablero, Num, Fila, 1, Salida).

chequeoFila(Tablero, _, Tablero).

% chequeoGrupo(+Tablero, +Num, +Fila, +IndexInicio, -Salida).
% Controla en Fila si hay un grupo de al menos 3, y efectua los reemplazos.
chequeoGrupo(Tablero, Num, [X | S], IndexInicio, Salida) :-
    X \= x,
    memberSeguido([X | S], X, CantidadSeguidas),
    CantidadSeguidas >= 3,
    IndexFin is IndexInicio + CantidadSeguidas - 1,
    chequeoCruzados(Tablero, Num, IndexInicio, IndexFin, X, SalidaCruzados),
    Salida = SalidaCruzados.

chequeoGrupo(Tablero, Num, [_ | S], IndexInicio, Salida) :-
    length(S, Largo),
    Largo > 2,
    IndexFin is IndexInicio + 1,
    chequeoGrupo(Tablero, Num, S, IndexFin, Salida).

% reemplazoGrupo(+Tablero, +Num, +IndexInicio, +IndexFinal, +Elemento, -Salida).
% Reemplaza en tablero, en la fila Num desde IndexInicio hasta IndexFinal con x o Evoluciones segun corresponda.
reemplazoGrupo(Tablero, Num, IndexInicio, IndexFinal, Elemento, Salida) :-
    memberIn(Num, Tablero, Fila),
    reemplazoCascara(Fila, IndexInicio, IndexFinal, Elemento, SalidaCascara),
    replace(Num, Tablero, SalidaCascara, SalidaReplace),
    Salida = SalidaReplace.

% reemplazoCascara(+Fila, +IndexInicio, +IndexFinal, +Elemento, -Salida).
% Predicado recursivo auxiliar para reemplazoGrupo/6
reemplazoCascara(Fila, IndexInicio, IndexFinal, Elemento, Salida) :-
    IndexInicio \= IndexFinal,
    IndexSig is IndexInicio + 1,
    IndexSig \= IndexFinal,
    replace(IndexInicio, Fila, x, NuevaFilaInicio),
    replace(IndexFinal, NuevaFilaInicio, x, NuevaFilaFin),
    NuevoInicio is IndexInicio + 1,
    NuevoFin is IndexFinal - 1,
    reemplazoCascara(NuevaFilaFin, NuevoInicio, NuevoFin, Elemento, Salida).

reemplazoCascara(Fila, IndexInicio, IndexFinal, Elemento, Salida) :-
    IndexSig is IndexInicio + 1,
    IndexSig == IndexFinal,
    evolucion(Elemento, EE),
    replace(IndexInicio, Fila, EE, NuevaFila),
    replace(IndexFinal, NuevaFila, x, NuevaFilaFin),
    Salida = NuevaFilaFin.

reemplazoCascara(Fila, IndexInicio, IndexFinal, Elemento, Salida) :-
    IndexInicio == IndexFinal,
    evolucion(Elemento, EE),
    replace(IndexInicio, Fila, EE, NuevaFila),
    Salida = NuevaFila.

% chequeoCruzados(+Tablero, +Num, +IndexInicio, +IndexFinal, +Elemento, -Salida).
% Controla si luego de un desplazamiento se produce un colapso cruzado, y luego reemplaza segun corresponda.
chequeoCruzados(Tablero, Num, IndexInicio, IndexFinal, Elemento, Salida) :-
    length(Tablero, L),

    trasponer(Tablero, L, TableroTraspuesto),
    chequeoDeColumnasCruzado(TableroTraspuesto, Num, Elemento, IndexInicio, IndexFinal, IndexInicioEnColumna, IndexColumna, CantidadSeguidosColumna),

    % Reemplaza en la columna de IndexColumna a partir de IndeXInicioEnColumna
    memberIn(IndexColumna, TableroTraspuesto, Columna),
    IndexFin is IndexInicioEnColumna + CantidadSeguidosColumna - 1,
    reemplazoCascara(Columna, IndexInicioEnColumna, IndexFin, Elemento, ColumnaNueva),

    % Evoluciono el elemento
    evolucion(Elemento, ElementoEvolucion),

    % Corrijo la columna
    encontrarPrimerAparicion(ElementoEvolucion, ColumnaNueva, IndexElementoEvolucion),
    replace(IndexElementoEvolucion, ColumnaNueva, x, ColumnaCorregida),

    % Reemplazo la columna corregida en el tablero
    replace(IndexColumna, TableroTraspuesto, ColumnaCorregida, TableroTraspuestoCorregido),

    % Obtengo la fila que tiene una x por los reemplazos previos
    trasponer(TableroTraspuestoCorregido, L, TableroCorregido),
    memberIn(Num, TableroCorregido, Fila),
    reemplazoFilaCruzado(Fila, IndexInicio, IndexFinal, ElementoEvolucion, FilaReemplazada),
    replace(Num, TableroCorregido, FilaReemplazada, TableroCorregidoReemplazado),
    
    Salida = TableroCorregidoReemplazado.

chequeoCruzados(Tablero, Num, IndexInicio, IndexFinal, Elemento, Salida) :-
    reemplazoGrupo(Tablero, Num, IndexInicio, IndexFinal, Elemento, Salida).

% reemplazoFilaCruzado(+Fila, +Inicio, +Final, +ElementoEvolucion, -SalidaFila)
% Reemplaza en la fila por x o ElementoEvolucion segun corresponda, asumiendo que se produjo
%   un colapso cruzado.
reemplazoFilaCruzado(Fila, Inicio, Final, ElementoEvolucion, SalidaFila) :-
    Inicio =< Final,
    memberIn(Inicio, Fila, ElementoEnInicio),
    ElementoEnInicio == x,
    replace(Inicio, Fila, ElementoEvolucion, FilaReemplazada),
    NuevoInicio is Inicio + 1,
    reemplazoFilaCruzado(FilaReemplazada, NuevoInicio, Final, ElementoEvolucion, SalidaFila).

reemplazoFilaCruzado(Fila, Inicio, Final, ElementoEvolucion, SalidaFila) :-
    Inicio =< Final,
    replace(Inicio, Fila, x, FilaReemplazada),
    NuevoInicio is Inicio + 1,
    reemplazoFilaCruzado(FilaReemplazada, NuevoInicio, Final, ElementoEvolucion, SalidaFila).

reemplazoFilaCruzado(Fila, Inicio, Final, _ElementoEvolucion, Fila) :- Inicio > Final.

% existeGrupo(-Elemento, -Lista, +IndexInicio).
% Controla si existe un grupo de al menos 3 Elemento's seguidos.
existeGrupo(Elemento, [Elemento | Resto], IndexInicio) :-
    memberSeguido([Elemento | Resto], Elemento, Cantidad),
    Cantidad > 2,
    IndexInicio is 1.

existeGrupo(Elemento, [Elemento | Resto], IndexInicio) :-
    length(Resto, L),
    L > 2,
    existeGrupo(Elemento, Resto, IndexAux),
    IndexInicio is IndexAux + 1.

existeGrupo(Elemento, Lista, IndexInicio) :- 
    encontrarPrimerAparicion(Elemento, Lista, Index),
    copiarNElementos(Index, Lista, _, Pos),
    existeGrupo(Elemento, Pos, _),
    IndexInicio is Index.


% chequeoGrupoConIndexEnLista(-Lista, -IndexFila, -Elemento, +SalidaIndexInicio, -Cantidad).
% Controla si existe un grupo cruzado en una lista, dado un index de Fila.
chequeoGrupoConIndexEnLista(Lista, IndexFila, Elemento, SalidaIndexInicio, CantidadSeguidos) :-
    existeGrupo(Elemento, Lista, IndexInicio),
    IndexCorregido is IndexInicio - 1,
    copiarNElementos(IndexCorregido, Lista, _, PosGrupo),
    memberSeguido(PosGrupo, Elemento, Cantidad),
    IndexInicio =< IndexFila,

    IndexFin is IndexInicio + Cantidad,

    IndexFin >= IndexFila,
    
    CantidadSeguidos is Cantidad,
    SalidaIndexInicio = IndexInicio.

% chequeoDeColumnasCruzado(+TableroTraspuesto, +IndexFila, +Elemento, +IndexInicioGrupo, +IndexFinGrupo, -SalidaIndexEnColumna, -SalidaIndexColumna, -Cantidad).
% Controla si hay un colapso cruzado con las columnas comprendidas entre IndexInicioGrupo e IndexFinGrupo,
%   y con la fila ubicada en IndexFila.
chequeoDeColumnasCruzado(TableroTraspuesto, IndexFila, Elemento, IndexInicioGrupo, IndexFinGrupo, SalidaIndexEnColumna, SalidaIndexColumna, Cantidad) :-
    IndexInicioGrupo =< IndexFinGrupo,
    memberIn(IndexInicioGrupo, TableroTraspuesto, Columna),
    chequeoGrupoConIndexEnLista(Columna, IndexFila, Elemento, SalidaIndexInicioEnColumna, CantidadSeguidosColumna),
    SalidaIndexEnColumna is SalidaIndexInicioEnColumna,
    SalidaIndexColumna is IndexInicioGrupo,
    Cantidad is CantidadSeguidosColumna.

chequeoDeColumnasCruzado(TableroTraspuesto, IndexFila, Elemento, IndexInicioGrupo, IndexFinGrupo, SalidaIndexEnColumna, SalidaIndexColumna, Cantidad) :-
    NuevoIndexInicioGrupo is IndexInicioGrupo + 1,
    NuevoIndexInicioGrupo =< IndexFinGrupo,
    chequeoDeColumnasCruzado(TableroTraspuesto, IndexFila, Elemento, NuevoIndexInicioGrupo, IndexFinGrupo, SalidaIndexEnColumna, SalidaIndexColumna, Cantidad).


% chequeoColumnas(+Tablero, +Num, -Salida).
% Corrobora todas las columnas de Tablero y devuelve 
%   en Salida el tablero con los colapsos correspondientes.
chequeoColumnas(TableroTraspuesto, 1, Salida) :- chequeoFila(TableroTraspuesto, 1, Salida).

chequeoColumnas(TableroTraspuesto, Num, Salida) :-
    chequeoFila(TableroTraspuesto, Num, SalidaChequeoFila),
    NumNuevo is Num - 1,
    chequeoColumnas(SalidaChequeoFila, NumNuevo, Salida).


/**
 * Gravedad
 **/

% gravedadEnTablero(+Tablero, -Salida).
% Asume que Tablero es una coleccion de columnas.
% Aplica la caida de fichas a huecos vacios en todo Tablero de manera recursiva.
gravedadEnTablero([X], Salida) :-
    gravedadEnColumna(X, SalidaReal),
    Salida = [SalidaReal].

gravedadEnTablero([X | Xs], Salida) :-
    gravedadEnColumna(X, SalidaColumna),
    gravedadEnTablero(Xs, SalidaRecursiva),
    concat([SalidaColumna], SalidaRecursiva, Salida).

% gravedadEnColumna(+Columna, -Salida).
% Efectua caida de todas las fichas al hueco vacio, si existe.
gravedadEnColumna(Columna, Salida) :-
    member(x, Columna),
    encontrarUltimaAparicion(x, Columna, N),
    copiarNElementos(N, Columna, ColumnaCortada, RestoColumna),
    desplazamientoDeLista(der, 1, ColumnaCortada, ColumnaCortadaDesplazada),
    concat(ColumnaCortadaDesplazada, RestoColumna, [Primer | RestoColumnaDesplazada]),
    
    gravedadEnColumna(RestoColumnaDesplazada, SalidaRecursiva),
    
    concat([Primer], SalidaRecursiva, Salida).

gravedadEnColumna(Columna, Columna).

/**
 * Rellenar
 **/

% rellenarCascara(+TableroCaido, +Cantidad, -TableroRelleno).
% Recorre el tablero Cantidad veces y devuelve el tablero con
%   las x intercambiadas por elementos aleatorios.
rellenarCascara(TableroCaido, 1, TableroRelleno) :-
    memberIn(1, TableroCaido, Fila),
    reemplazarX(Fila, FilaNueva),
    replace(1, TableroCaido, FilaNueva, TableroNuevo),

    TableroRelleno = TableroNuevo.

rellenarCascara(TableroCaido, Cantidad, TableroRelleno) :-
    CantidadNueva is Cantidad - 1,

    rellenarCascara(TableroCaido, CantidadNueva, SalidaRellenoAux),

    memberIn(Cantidad, SalidaRellenoAux, Fila),
    reemplazarX(Fila, FilaNueva),
    replace(Cantidad, SalidaRellenoAux, FilaNueva, TableroNuevo),

    TableroRelleno = TableroNuevo.

% reemplazarX(+Fila, -FilaReemplazada).
% Recibe la Fila y reemplaza todas las apariciones de x por
%   un elemento aleatorio.
reemplazarX(Fila, FilaReemplazada) :-
    length(Fila, Cantidad),
    reemplazarXAux(Fila, 1, Cantidad, FilaConReemplazos),
    FilaReemplazada = FilaConReemplazos.

% reemplazarXAux(+Fila, +Index, +CantidadFinal, -FilaReemplazada).
% Metodo cascara que efectua el intercambio de cada x por un elemento aleatorio.
reemplazarXAux(Fila, Index, CantidadFinal, FilaReemplazada) :-
    Index =< CantidadFinal,
    memberIn(Index, Fila, Elemento),
    Elemento == x,
    obtenerElementoAleatorio(ElementoAleatorio),
    replace(Index, Fila, ElementoAleatorio, FilaConReemplazo),
    IndexSiguiente is Index + 1,
    reemplazarXAux(FilaConReemplazo, IndexSiguiente, CantidadFinal, FilaConReemplazo),
    FilaReemplazada = FilaConReemplazo.

reemplazarXAux(Fila, Index, CantidadFinal, FilaReemplazada) :-
    Index =< CantidadFinal,
    IndexSiguiente is Index + 1,
    reemplazarXAux(Fila, IndexSiguiente, CantidadFinal, FilaConReemplazo),
    FilaReemplazada = FilaConReemplazo.

reemplazarXAux(Fila, Index, CantidadFinal, Fila) :- Index > CantidadFinal.
